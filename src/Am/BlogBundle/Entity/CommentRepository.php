<?php

namespace Am\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CommentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CommentRepository extends EntityRepository
{
	public function getCommentFromArticle($article){
		//DQL Request
		$em = $this->getDoctrine()->getEntityManager();
        $query = $em->createQuery(
            'SELECT p FROM AmBlogBundle:Comment p WHERE p.article_id='.$article->getId()
        );

        $comments = $query->getResult();

        return $comments;
	}
}
