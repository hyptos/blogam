<?php

namespace Am\BlogBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
 
/**
 * Am\BlogBundle\Entity\Comment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Am\BlogBundle\Entity\CommentRepository")
 */
class Comment
{
  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;
 
  /**
   * @var string $author
   *
   * @ORM\Column(name="author", type="string", length=255)
   */
  private $author;
 
  /**
   * @var text $content
   *
   * @ORM\Column(name="content", type="text")
   */
  private $content;
 
  /**
   * @var datetime $date
   *
   * @ORM\Column(name="date", type="datetime")
   */
  private $date;

  /**
  * @ORM\ManyToOne(targetEntity="Am\BlogBundle\Entity\Article")
  * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
  */
  private $article;
 
  public function __construct()
  {
    $this->date = new \Datetime();
  }
 
  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }
 
  /**
   * Set author
   *
   * @param string $author
   */
  public function setAuthor($author)
  {
    $this->author = $author;
  }
 
  /**
   * Get author
   *
   * @return string
   */
  public function getAuthor()
  {
  return $this->author;
  }
 
  /**
   * Set content
   *
   * @param text $content
   */
  public function setContent($content)
  {
    $this->content = $content;
  }
 
  /**
   * Get content
   *
   * @return text
   */
  public function getContent()
  {
    return $this->content;
  }
 
  /**
   * Set date
   *
   * @param datetime $date
   */
  public function setDate(\Datetime $date)
  {
    $this->date = $date;
  }
 
  /**
   * Get date
   *
   * @return datetime
   */
  public function getDate()
  {
    return $this->date;
  }

    /**
     * Set article
     *
     * @param \Am\BlogBundle\Entity\Article $article
     * @return Comment
     */
    public function setArticle(\Am\BlogBundle\Entity\Article $article)
    {
        $this->article = $article;
    
        return $this;
    }

    /**
     * Get article
     *
     * @return \Am\BlogBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}