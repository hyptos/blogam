<?php

namespace Am\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Am\BlogBundle\Entity\Article;
use Am\BlogBundle\Entity\Image;
use Am\BlogBundle\Entity\Comment;



class BlogController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $query = $em->createQuery(
            'SELECT p FROM AmBlogBundle:Article p'
        )->setMaxResults(5);

        $articles = $query->getResult();

        return $this->render('AmBlogBundle:Blog:index.html.twig', array('articles'=>$articles));
    }

    /* ----------- MANGEMENT OF ARTICLE ----------*/

    public function editArticleAction()
    {

    }

    public function delArticleAction(Article $article)
    {

        $em = $this->getDoctrine()
                   ->getEntityManager();

        $list_tags = $em->getRepository('AmBlogBundle:Tag')
                        ->findAll();               

        //In order to delete an article, we have to remove each object linked to an Article
        foreach($list_tags as $value){
            $article->removeTags($value);
        }

        // We remove the Article
        $em = $this->getDoctrine()->getManager();
        $em->remove($article); 
        $em->flush();

        return $this->render('AmBlogBundle:Blog:delArticle.html.twig');
    }

    public function viewArticleAction(Article $article)
    {
         // We GET the article
        $em = $this->getDoctrine()
                   ->getEntityManager();
     
        if($article === null)
        {
          throw $this->createNotFoundException('Article[id='.$id.'] inexistant.');
        }

         // We CREATE the form for comment
        $comment = new Comment;

        $form = $this->createFormBuilder($comment)
            ->add('content','textarea')
            ->getForm();

        // On récupère la requête.
        $request = $this->get('request');

        if( $request->getMethod() == 'POST' )
        {
            $form->bind($request);

            if( $form->isValid() )
            {
                $date   = new \DateTime("now"); 
                $comment->setAuthor('Admin');
                $comment->setDate($date);
                $comment->setArticle($article);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($comment);
                $em->flush();

                //Reponse ajax
                return new response('Le commentaire a été ajouter !');
            }
        }
     
        // On GET the list of comments
        
        //Appel au repository afin de récupérerles commentaires correspondant a l'article


        if($list_comments === null)
        {
          throw $this->createNotFoundException('Commentaires inexistant.');
        }                   

        return $this->render('AmBlogBundle:Blog:viewArticle.html.twig', array(
          'article'         => $article,
          'list_comments'   => $list_comments,
          'form'            => $form->createView()
        ));
    }

    public function addArticleAction()
    {                 
        $article = new Article;

        $form = $this->createFormBuilder($article)
            ->add('title','text')
            ->add('content','textarea')
            ->getForm();

        // On récupère la requête.
        $request = $this->get('request');

            if( $request->getMethod() == 'POST' )
            {
                $form->bind($request);

                if( $form->isValid() )
                {
                    $date   = new \DateTime("now"); 
                    $article->setAuthor('Admin');
                    $article->setDate($date);

                    $em = $this->getDoctrine()->getEntityManager();
                    $em->persist($article);
                    $em->flush();

                    //Reponse flash
                    return new response('OK');
                }
            }

            return $this->render('AmBlogBundle:Blog:addArticle.html.twig', array(
              'form'         => $form->createView(),
            ));
    }

    /* ----------- MANGEMENT OF ARTICLE & COMMENT ----------*/

    public function addCommentToArticle(Article $article,Comment $comment)
    {
        //Add the comment to the article
        $comment->setArticle($article);

        //Save
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment); 
        $em->flush();        

    }

    public function delCommentToArticle(Article $article,Comment $comment)
    {       
        //Remove the comment to the article
        $comment->removeArticle($article);

        //Save
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment); 
        $em->flush();        

        return new Response('OK');

    }


}
